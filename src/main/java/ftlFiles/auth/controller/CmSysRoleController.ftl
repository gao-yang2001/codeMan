package <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.core.annotation.LoginRequired;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.core.annotation.RecordLog;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.entity.CmSysButtonEntity;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.entity.CmSysMenuEntity;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.entity.CmSysRoleEntity;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.entity.CmSysUserEntity;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.entity.PageData;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.service.CmSysRoleService;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.utils.SessionUtil;
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.utils.SnowflakeIdWorker;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
@Api(tags = "cmSysRole接口")
@RequestMapping("/cmSysRole")
public class CmSysRoleController {

	private final CmSysRoleService service;

	@Autowired
	public CmSysRoleController(CmSysRoleService service) {
		this.service = service;
	}

	/**
	 * 根据id查询用户所拥有的的菜单
	 *
	 * @return
	 */
	@ApiOperation(value = "根据id查询角色所拥有的的菜单")
	@LoginRequired
	@RecordLog
	@GetMapping("/{roleId}/menus")
	public List<CmSysMenuEntity> getMenusById(@PathVariable Long roleId) {
		return service.getMenusById(roleId);
	}

	/**
	 * 根据id查询用户所拥有的的菜单
	 *
	 * @return
	 */
	@ApiOperation(value = "根据id查询角色所拥有的的按钮")
	@LoginRequired
	@RecordLog
	@GetMapping("/{roleId}/buttons")
	public Map<Long, List<CmSysButtonEntity>> getButtonsById(@PathVariable Long roleId) {
		return service.getButtonsById(roleId);
	}

	/**
	 * 查询全部角色列表
	 *
	 * @return
	 */
	@ApiOperation(value = "查询全部角色列表")
	@LoginRequired
	@RecordLog
	@GetMapping(value = "/list-all")
	public List<CmSysRoleEntity> listAll() {
		return service.listAll();
	}

	/**
	 * 根据id查询
	 *
	 * @return
	 */
	@ApiOperation(value = "根据id查询")
	@LoginRequired
	@RecordLog
	@GetMapping("/{roleId}")
	public CmSysRoleEntity getById(@PathVariable Long roleId) {
		return service.getById(roleId);
	}


	/**
	 * 模糊查询
	 *
	 * @return
	 */
	@ApiOperation(value = "模糊查询")
	@LoginRequired
	@RecordLog
	@PostMapping(value = "/likeSelect")
	public PageData<CmSysRoleEntity> likeSelect(@RequestBody CmSysRoleEntity entity) {
		return service.likeSelect(entity);
	}

	/**
	 * 更新
	 *
	 * @return
	 */
	@ApiOperation(value = "更新")
	@LoginRequired
	@RecordLog
	@PostMapping(value = "/update")
	public void update(@RequestBody CmSysRoleEntity entity, HttpServletRequest request) {
		CmSysUserEntity user = SessionUtil.getUser(request);
		entity.setUpdateTime(new Date());
		entity.setUpdateUserId(user.getUserId());
		service.update(entity);
	}

	/**
	 * 添加
	 *
	 * @return
	 */
	@ApiOperation(value = "添加")
	@LoginRequired
	@RecordLog
	@PostMapping(value = "/add")
	public void add(@RequestBody CmSysRoleEntity entity, HttpServletRequest request) {
		CmSysUserEntity user = SessionUtil.getUser(request);
		entity.setCreateTime(new Date());
		entity.setCreateUserId(user.getUserId());
		entity.setRoleId(SnowflakeIdWorker.generateId());
		service.add(entity);
	}

	/**
	 * 删除
	 *
	 * @return
	 */
	@ApiOperation(value = "删除")
	@LoginRequired
	@RecordLog
	@PostMapping(value = "/delete")
	public void delete(@RequestBody CmSysRoleEntity entity) {
		service.delete(entity);
	}


}
