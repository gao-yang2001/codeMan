package zuul;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
<#if cloudRegisteCenter=="eureka">
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
</#if>
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * 网关zuul（现在推荐使用gateway）
 */
@SpringBootApplication
<#if cloudRegisteCenter=="eureka">
@EnableEurekaClient
</#if>
@EnableDiscoveryClient
@EnableZuulProxy
public class CloudZuul {

    public static void main(String[] args) {
        SpringApplication.run(CloudZuul.class);
    }


}
