package feign.clients.${packageName};


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

<#if !entityName??>
import java.util.Map;
import org.springframework.web.bind.annotation.RequestParam;
<#else>
import <#if isCloudModel>${cloudSysEngName}<#else>${packageName}</#if>.entity.${entityName};
</#if>
import java.util.List;

/**
* @author zrx
*/
@FeignClient(value = "${projectName}-service", path = "/${controllerPrefix}")
public interface ${controllerPrefix?cap_first}Client {

<#if !entityName??>
    /**
    * 查询
    *
    * @return
    */
    @PostMapping(value = "/select")
    List<Map<String, Object>> select(@RequestBody Map<String, Object> map);

    /**
    * 更新
    *
    * @return
    */
    @PostMapping(value = "/update")
    void update(@RequestBody Map<String, Object> map);

    /**
    * 添加
    *
    * @return
    */
    @PostMapping(value = "/add")
    void add(@RequestBody Map<String, Object> map);

    /**
    * 删除
    *
    * @return
    */
    @PostMapping(value = "/delete")
    void delete(@RequestBody Map<String, Object> map);
<#else>
    /**
	 * 查询
	 *
	 * @return
	 */
	@PostMapping(value = "/select")
	List<${entityName}> select(@RequestBody ${entityName} entity);

	/**
	 * 更新
	 *
	 * @return
	 */
	@PostMapping(value = "/update")
	void update(@RequestBody ${entityName} entity);

	/**
	 * 添加
	 *
	 * @return
	 */
	@PostMapping(value = "/add")
	void add(@RequestBody ${entityName} entity);

	/**
	 * 删除
	 *
	 * @return
	 */
	@PostMapping(value = "/delete")
	void delete(@RequestBody ${entityName} entity);
</#if>

}
