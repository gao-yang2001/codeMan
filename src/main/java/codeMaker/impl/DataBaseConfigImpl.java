package codeMaker.impl;

import constant.ChildWindowConstant;
import constant.CodeConstant;
import constant.CompareComboBox;
import constant.QueryCheckBox;
import constant.ServiceJTextField;
import constant.ServiceTypeComboBox;
import constant.ShowNumJSpinner;
import constant.UpdateCheckBox;
import entity.DataSourceModel;
import entity.DatabaseModel;
import entity.Parameters;
import entity.TableNameAndType;
import util.DBUtils;
import util.DataUtils;

import javax.swing.*;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.NumberFormatter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @ClassName DataBaseConfigImpl
 * @Author zrx
 * @Date 2021/11/23 17:53
 */
public class DataBaseConfigImpl {

	private static volatile DataBaseConfigImpl dataBaseConfigImpl;

	public static DataBaseConfigImpl getInstance() {
		if (null == dataBaseConfigImpl) {
			synchronized (DataBaseConfigImpl.class) {
				if (null == dataBaseConfigImpl) {
					dataBaseConfigImpl = new DataBaseConfigImpl();
				}
			}
		}
		return dataBaseConfigImpl;
	}

	/**
	 * 确认 listener
	 *
	 * @param primaryKeyListText
	 * @param currentTableCn
	 * @param parameters
	 * @param confirmButton
	 * @param tableNameList
	 * @param paramConfig
	 * @param columnNamePanel
	 */
	public void setConfirmListener(JTextField primaryKeyListText, JTextField currentTableCn, Parameters parameters, JButton confirmButton, JComboBox<String> tableNameList, JComboBox<String> paramConfig, JPanel columnNamePanel) {
		confirmButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// 获取当前选择的表名
				String currentTableName = Objects.requireNonNull(tableNameList.getSelectedItem()).toString();
				// 什么也不做
				if ("-请选择-".equals(currentTableName)) {
					return;
				}
				//如果当前表没有设置过实体属性
				if (ChildWindowConstant.currentTableNameAndTypes.get(currentTableName) == null) {
					// 查询表名所有字段的名称和类型
					List<TableNameAndType> currentTableNameAndTypes = DBUtils.getColumnNameAndTypes(
							parameters.getDataBaseTypeVal(), currentTableName, DBUtils.getConnection(parameters));
					//把当前表的字段名和类型设置到全局map中
					ChildWindowConstant.currentTableNameAndTypes.put(currentTableName, currentTableNameAndTypes);
				}
				// 检查当前表的主键是否填写
				if ("".equals(primaryKeyListText.getText())) {
					JOptionPane.showMessageDialog(confirmButton.getParent(), "请填写当前表的主键！如果没有主键，填写一个唯一键或其他任意列即可", "警告",
							JOptionPane.WARNING_MESSAGE);
					return;
				}

				// 设置当前表的主键
				List<String> priamryKeyArr = Arrays.asList(primaryKeyListText.getText().split("#"));
				ChildWindowConstant.primaryKeyListMap.put(currentTableName, priamryKeyArr);
				// 设置当前表的中文名称
				String currentTableCnName = "".equals(currentTableCn.getText()) ? currentTableName
						: currentTableCn.getText();
				//改为
				ChildWindowConstant.currentTableCnNameMap.put(currentTableName, currentTableCnName);

				// 设置表的参数类型
				String selectParam = Objects.requireNonNull(paramConfig.getSelectedItem()).toString();
				ChildWindowConstant.tableParamConfig.put(CodeConstant.PARAM_CONFIG_KEY, selectParam);

				Component[] components = columnNamePanel.getComponents();

				List<DatabaseModel> columnMsgList = new ArrayList<>();

				// 从第9个开始遍历
				for (int i = 10; i < components.length; i++) {

					if ("JCheckBox".equals(components[i].getClass().getSimpleName())) {

						JCheckBox checkBox = (JCheckBox) components[i];

						DatabaseModel databaseModel = new DatabaseModel();
						databaseModel.setCheckBox(checkBox);
						columnMsgList.add(databaseModel);

					} else if ("JTextField".equals(components[i].getClass().getSimpleName())) {

						JTextField textField = (JTextField) components[i];
						columnMsgList.get(columnMsgList.size() - 1).setTextField(textField);

					} else if ("JSpinner".equals(components[i].getClass().getSimpleName())) {

						JSpinner jSpinner = (JSpinner) components[i];
						columnMsgList.get(columnMsgList.size() - 1).setjSpinner(jSpinner);

					} else if ("ShowNumJSpinner".equals(components[i].getClass().getSimpleName())) {

						ShowNumJSpinner showNumJspinner = (ShowNumJSpinner) components[i];
						columnMsgList.get(columnMsgList.size() - 1).setShowNumJSpinner(showNumJspinner);

					} else if ("JComboBox".equals(components[i].getClass().getSimpleName())) {

						JComboBox<String> comboBox = (JComboBox<String>) components[i];
						columnMsgList.get(columnMsgList.size() - 1).setComboBox(comboBox);

					}

					// 如果是业务类型下拉框
					else if ("ServiceTypeComboBox".equals(components[i].getClass().getSimpleName())) {
						ServiceTypeComboBox<String> comboBox = (ServiceTypeComboBox<String>) components[i];
						columnMsgList.get(columnMsgList.size() - 1).setServiceTypeComboBox(comboBox);
					}

					// 如果是业务类型描述
					else if ("ServiceJTextField".equals(components[i].getClass().getSimpleName())) {
						ServiceJTextField textField = (ServiceJTextField) components[i];
						columnMsgList.get(columnMsgList.size() - 1).setServiceJTextField(textField);

					} else if ("CompareComboBox".equals(components[i].getClass().getSimpleName())) {

						CompareComboBox<String> comboBox = (CompareComboBox<String>) components[i];
						columnMsgList.get(columnMsgList.size() - 1).setCompareComboBox(comboBox);

					} else if ("UpdateCheckBox".equals(components[i].getClass().getSimpleName())) {

						UpdateCheckBox updateCheckBox = (UpdateCheckBox) components[i];
						columnMsgList.get(columnMsgList.size() - 1).setUpdateCheckBox(updateCheckBox);

					} else if ("QueryCheckBox".equals(components[i].getClass().getSimpleName())) {

						QueryCheckBox queryCheckBox = (QueryCheckBox) components[i];
						columnMsgList.get(columnMsgList.size() - 1).setQueryCheckBox(queryCheckBox);
					}

				}

				// 前台需要展示的数据项
				List<DatabaseModel> selectColumnList = new ArrayList<>();

				// 前台更新用于展示的数据项
				List<DatabaseModel> updateColumnList = new ArrayList<>();

				// 前台查询用于展示的数据项
				List<DatabaseModel> queryColumnList = new ArrayList<>();

				// 以选中的checkBox为基准
				for (DatabaseModel databaseModel : columnMsgList) {
					JCheckBox checkColumnName = databaseModel.getCheckBox();
					JTextField textField = databaseModel.getTextField();
					//需要设置，不然反序列化会失败，很诡异，估计跟对象状态有关
					textField.setText(textField.getText());
					JSpinner jSpinner = databaseModel.getjSpinner();
					JSpinner showNumJspinner = databaseModel.getShowNumJSpinner();
					JComboBox<String> comboBox = databaseModel.getComboBox();
					ServiceTypeComboBox<String> serviceTypeComboBox = databaseModel.getServiceTypeComboBox();
					ServiceJTextField serviceJtextField = databaseModel.getServiceJTextField();
					//需要设置，不然反序列化会失败，很诡异，估计跟对象状态有关
					serviceJtextField.setText(serviceJtextField.getText());
					CompareComboBox<String> compareComboBox = databaseModel.getCompareComboBox();
					UpdateCheckBox updateCheckBox = databaseModel.getUpdateCheckBox();
					QueryCheckBox queryCheckBox = databaseModel.getQueryCheckBox();

					String columnEng = checkColumnName.getText();
					databaseModel.setColumnsEng(columnEng);
					//设置SqlParamColumnEng
					databaseModel.setSqlParamColumnEng(DataUtils.getSqlParam(columnEng));
					databaseModel.setColumnsCn("".equals(textField.getText()) ? columnEng : textField.getText());
					databaseModel.setWhereNo((Integer) jSpinner.getValue());
					databaseModel.setShowNo((Integer) showNumJspinner.getValue());
					databaseModel.setCanSort((String) comboBox.getSelectedItem());

					String selectedItem = (String) serviceTypeComboBox.getSelectedItem();
					databaseModel.setServiceType(selectedItem);

					String serviceText = serviceJtextField.getText();

					databaseModel.setServiceTextStr(serviceText);

					String[] serviceArr = serviceText.split("#");

					databaseModel.setCompareValue((String) compareComboBox.getSelectedItem());

					if (checkType(databaseModel, selectedItem, serviceArr)) {
						JOptionPane.showMessageDialog(confirmButton.getParent(),
								columnEng + "的类型描述填写有误，请检查后按照配置说明重新填写！", "错误", JOptionPane.ERROR_MESSAGE);
						return;
					}

					if (checkColumnName.isSelected()) {
						selectColumnList.add(databaseModel);
					}

					if (checkColumnName.isSelected() && updateCheckBox.isSelected()) {
						updateColumnList.add(databaseModel);
					}

					if (queryCheckBox.isSelected()) {
						queryColumnList.add(databaseModel);
					}

				}

				// 设置完毕后查看checkBox的集合是否为空
				if (selectColumnList.size() == 0) {
					JOptionPane.showMessageDialog(confirmButton.getParent(),
							"你没有选择表 " + currentTableName + " 需要展示的数据项！生成时默认为你全选且中文名为英文字段名称！", "警告",
							JOptionPane.WARNING_MESSAGE);

				}

				if (updateColumnList.size() == 0) {
					JOptionPane.showMessageDialog(confirmButton.getParent(), "你没有选择页面 更新功能 需要展示的数据项！生成时将以展示的数据项为准！",
							"警告", JOptionPane.WARNING_MESSAGE);

				}

				if (queryColumnList.size() == 0) {
					JOptionPane.showMessageDialog(confirmButton.getParent(), "你没有选择页面 查询功能 需要展示的数据项！生成时将以展示的数据项为准！",
							"警告", JOptionPane.WARNING_MESSAGE);

				}

				JOptionPane.showMessageDialog(confirmButton.getParent(), "表 " + currentTableName + " 数据项配置成功！", "提示",
						JOptionPane.INFORMATION_MESSAGE);

				// 展示项的信息
				ChildWindowConstant.columnMsgMap.put(currentTableName, selectColumnList);
				// 更新项的信息
				ChildWindowConstant.updateColumnMsgMap.put(currentTableName, updateColumnList);
				// 查询项的信息
				ChildWindowConstant.queryColumnMsgMap.put(currentTableName, queryColumnList);
				// 所有项的信息
				ChildWindowConstant.allColumnMsgMap.put(currentTableName, columnMsgList);
			}

			private boolean checkType(DatabaseModel databaseModel, String selectedItem,
									  String[] serviceArr) {
				Map<String, String> map = new HashMap<>();
				if ("布尔".equals(selectedItem)) {
					try {
						map.put("是", serviceArr[0]);
						map.put("否", serviceArr[1]);
						databaseModel.setServiceText(map);
					} catch (Exception e1) {
						return true;
					}
				}

				if ("状态码".equals(selectedItem)) {
					try {
						for (String serviece : serviceArr) {
							String[] arr = serviece.split("&");
							map.put(arr[0], arr[1]);
						}
						databaseModel.setServiceText(map);
					} catch (Exception e1) {
						return true;
					}
				}
				return false;
			}
		});
	}


	/**
	 * 表名变更 listener
	 *
	 * @param primaryKeyListText
	 * @param currentTableCn
	 * @param parameters
	 * @param tableNameList
	 * @param paramConfig
	 * @param headNameArr
	 * @param columnNamePanel
	 */
	public void setTableNameListener(JTextField primaryKeyListText, JTextField currentTableCn, Parameters parameters, JComboBox<String> tableNameList, JComboBox<String> paramConfig, String[] headNameArr, JPanel columnNamePanel) {
		tableNameList.addItemListener(e -> {

			if (e.getStateChange() != ItemEvent.SELECTED) {
				return;
			}

			String currentTableName = tableNameList.getSelectedItem().toString();

			// 移除所有元素
			columnNamePanel.removeAll();

			// 如果选择了这一个，不做任何操作
			if ("-请选择-".equals(currentTableName)) {
				primaryKeyListText.setText("");
				currentTableCn.setText("");
				ChildWindowConstant.dataBaseConfig.validate();
				ChildWindowConstant.dataBaseConfig.repaint();
				return;
			}

			// 获取当前表主键
			List<String> primaryList = ChildWindowConstant.primaryKeyListMap.get(currentTableName);
			if (primaryList == null) {
				//动态设置主键
				try {
					primaryList = DBUtils.getPrimarykey(parameters, currentTableName);
					ChildWindowConstant.primaryKeyListMap.put(currentTableName, primaryList);
				} catch (Exception ignored) {
				}
			}

			// 获取当前表的中文名
			String currentTableCnName = ChildWindowConstant.currentTableCnNameMap.get(currentTableName);

			// 获取表的参数类型
			String paramFiled = ChildWindowConstant.tableParamConfig.get(CodeConstant.PARAM_CONFIG_KEY);
			String selectParam = paramFiled == null ? "JavaBean" : paramFiled;

			getAndSetPkStr(primaryKeyListText, primaryList);

			currentTableCn.setText(currentTableCnName == null ? "" : currentTableCnName);

			paramConfig.setSelectedItem(selectParam);

			List<DatabaseModel> databaseModelList = ChildWindowConstant.allColumnMsgMap.get(currentTableName);

			// 如果不为null，主键必然不为null 直接遍历全部的列添加即可
			if (databaseModelList != null) {
				columnNamePanel.setLayout(new GridLayout(0, 10, 10, 10));
				for (String headName : headNameArr) {
					JLabel jLabel = new JLabel(headName);
					columnNamePanel.add(jLabel);
				}
				for (DatabaseModel databaseModel : databaseModelList) {
					JCheckBox checkBox = databaseModel.getCheckBox();
					JTextField textField = databaseModel.getTextField();
					JSpinner jSpinner = databaseModel.getjSpinner();
					ShowNumJSpinner showNumJSpinner = databaseModel.getShowNumJSpinner();
					JComboBox<String> comboBox = databaseModel.getComboBox();
					ServiceTypeComboBox<String> serviceTypeComboBox = databaseModel.getServiceTypeComboBox();
					ServiceJTextField serviceJTextField = databaseModel.getServiceJTextField();
					CompareComboBox<String> compareComboBox = databaseModel.getCompareComboBox();
					UpdateCheckBox updateCheckBox = databaseModel.getUpdateCheckBox();
					QueryCheckBox queryCheckBox = databaseModel.getQueryCheckBox();

					setColumnNamePanelInfo(columnNamePanel, checkBox, textField, jSpinner, showNumJSpinner, comboBox, serviceTypeComboBox, serviceJTextField, compareComboBox, updateCheckBox, queryCheckBox);
				}

				ChildWindowConstant.dataBaseConfig.validate();
				ChildWindowConstant.dataBaseConfig.repaint();
				return;
			}

			// 如果需要查询数据库，则证明是首次设置，直接按最初的默认值添加即可
			List<TableNameAndType> columnNameList = DBUtils.getColumnNameAndTypes(parameters.getDataBaseTypeVal(),
					currentTableName, DBUtils.getConnection(parameters));
			columnNamePanel.setLayout(new GridLayout(0, 10, 10, 10));

			for (String headName : headNameArr) {
				JLabel jLabel = new JLabel(headName);
				columnNamePanel.add(jLabel);
			}

			// 动态展示列名
			assert columnNameList != null;
			for (TableNameAndType tableNameAndType : columnNameList) {
				String columnName = tableNameAndType.getName();
				JCheckBox columnCheckBox = new JCheckBox(columnName);
				JTextField column_cn = new JTextField(3);
				column_cn.setText(tableNameAndType.getComment());
				JComboBox<String> comboBox = new JComboBox<>();
				comboBox.setModel(new DefaultComboBoxModel<>(new String[]{"否", "是"}));
				// 字段优先级
				JSpinner spinner = initJspinner(new JSpinner());
				JSpinner showNumJSpinner = initJspinner(new ShowNumJSpinner());
				ServiceTypeComboBox<String> serviceTypeComboBox = new ServiceTypeComboBox<>();
				serviceTypeComboBox.setModel(
						new DefaultComboBoxModel<>(new String[]{"字符串", "数字", "布尔", "状态码", "日期", "文字域"}));
				ServiceJTextField serviceJTextField = new ServiceJTextField();
				CompareComboBox<String> compareComboBox = new CompareComboBox<>();
				compareComboBox.setModel(new DefaultComboBoxModel<>(
						new String[]{"=", "like", ">=", "<=", "!=", ">= && <="}));
				UpdateCheckBox updateCheckBox = new UpdateCheckBox(columnName);
				QueryCheckBox queryCheckBox = new QueryCheckBox(columnName);

				setColumnNamePanelInfo(columnNamePanel, columnCheckBox, column_cn, spinner, showNumJSpinner, comboBox, serviceTypeComboBox, serviceJTextField, compareComboBox, updateCheckBox, queryCheckBox);
			}

			ChildWindowConstant.dataBaseConfig.validate();
			ChildWindowConstant.dataBaseConfig.repaint();
		});
	}

	/**
	 * 刷新表结构 listener
	 *
	 * @param refreshTableBtn
	 * @param tableNameList
	 * @param columnNamePanel
	 */
	public void setRefreshTableListener(JTextField primaryKeyListText, JButton refreshTableBtn, Parameters parameters, JComboBox<String> tableNameList, String[] headNameArr, JPanel columnNamePanel) {
		refreshTableBtn.addActionListener(e -> {
			// 获取当前选择的表名
			String currentTableName = Objects.requireNonNull(tableNameList.getSelectedItem()).toString();
			// 什么也不做
			if ("-请选择-".equals(currentTableName)) {
				return;
			}
			// 重新设置当前表主键
			try {
				java.util.List<String> primaryList = DBUtils.getPrimarykey(parameters, currentTableName);
				ChildWindowConstant.primaryKeyListMap.put(currentTableName, primaryList);
				getAndSetPkStr(primaryKeyListText, primaryList);
			} catch (Exception ignored) {
			}
			//获取当前的所有字段
			java.util.List<DatabaseModel> databaseModelList = ChildWindowConstant.allColumnMsgMap.get(currentTableName);
			List<TableNameAndType> columnNameList = DBUtils.getColumnNameAndTypes(parameters.getDataBaseTypeVal(),
					currentTableName, DBUtils.getConnection(parameters));
			//更新该表的字段名称和类型
			ChildWindowConstant.currentTableNameAndTypes.put(currentTableName, columnNameList);
			// 移除所有元素
			columnNamePanel.removeAll();
			for (String headName : headNameArr) {
				JLabel jLabel = new JLabel(headName);
				columnNamePanel.add(jLabel);
			}
			// 动态展示列名
			assert columnNameList != null;

			for (TableNameAndType tableNameAndType : columnNameList) {
				String columnName = tableNameAndType.getName();
				DatabaseModel databaseModel = new DatabaseModel();
				databaseModel.setColumnsEng(columnName);
				JCheckBox checkBox;
				JTextField textField;
				JSpinner jSpinner;
				JSpinner showNumJSpinner;
				JComboBox<String> comboBox;
				ServiceTypeComboBox<String> serviceTypeComboBox;
				ServiceJTextField serviceJTextField;
				CompareComboBox<String> compareComboBox;
				UpdateCheckBox updateCheckBox;
				QueryCheckBox queryCheckBox;
				if (databaseModelList != null && databaseModelList.contains(databaseModel)) {
					DatabaseModel existModel = databaseModelList.get(databaseModelList.indexOf(databaseModel));
					checkBox = existModel.getCheckBox();
					textField = existModel.getTextField();
					jSpinner = existModel.getjSpinner();
					showNumJSpinner = existModel.getShowNumJSpinner();
					comboBox = existModel.getComboBox();
					serviceTypeComboBox = existModel.getServiceTypeComboBox();
					serviceJTextField = existModel.getServiceJTextField();
					compareComboBox = existModel.getCompareComboBox();
					updateCheckBox = existModel.getUpdateCheckBox();
					queryCheckBox = existModel.getQueryCheckBox();
				} else {
					checkBox = new JCheckBox(columnName);
					textField = new JTextField(3);
					textField.setText(tableNameAndType.getComment());
					jSpinner = initJspinner(new JSpinner());
					showNumJSpinner = initJspinner(new ShowNumJSpinner());
					comboBox = new JComboBox<>();
					comboBox.setModel(new DefaultComboBoxModel<>(new String[]{"否", "是"}));
					serviceTypeComboBox = new ServiceTypeComboBox<>();
					serviceTypeComboBox.setModel(
							new DefaultComboBoxModel<>(new String[]{"字符串", "数字", "布尔", "状态码", "日期", "文字域"}));
					serviceJTextField = new ServiceJTextField();
					compareComboBox = new CompareComboBox<>();
					compareComboBox.setModel(new DefaultComboBoxModel<>(
							new String[]{"=", "like", ">=", "<=", "!=", ">= && <="}));
					updateCheckBox = new UpdateCheckBox(columnName);
					queryCheckBox = new QueryCheckBox(columnName);
				}
				setColumnNamePanelInfo(columnNamePanel, checkBox, textField, jSpinner, showNumJSpinner, comboBox, serviceTypeComboBox, serviceJTextField, compareComboBox, updateCheckBox, queryCheckBox);
			}
			//重新绘制
			ChildWindowConstant.dataBaseConfig.validate();
			ChildWindowConstant.dataBaseConfig.repaint();
		});
	}

	/**
	 * 向中间的字段类表里填充元素
	 *
	 * @param columnNamePanel
	 * @param checkBox
	 * @param textField
	 * @param jSpinner
	 * @param showNumJSpinner
	 * @param comboBox
	 * @param serviceTypeComboBox
	 * @param serviceJTextField
	 * @param compareComboBox
	 * @param updateCheckBox
	 * @param queryCheckBox
	 */
	private void setColumnNamePanelInfo(JPanel columnNamePanel, JCheckBox checkBox, JTextField textField, JSpinner jSpinner, JSpinner showNumJSpinner, JComboBox<String> comboBox, ServiceTypeComboBox<String> serviceTypeComboBox, ServiceJTextField serviceJTextField, CompareComboBox<String> compareComboBox, UpdateCheckBox updateCheckBox, QueryCheckBox queryCheckBox) {
		columnNamePanel.add(checkBox);
		columnNamePanel.add(textField);
		columnNamePanel.add(jSpinner);
		columnNamePanel.add(showNumJSpinner);
		columnNamePanel.add(comboBox);
		columnNamePanel.add(serviceTypeComboBox);
		columnNamePanel.add(serviceJTextField);
		columnNamePanel.add(compareComboBox);
		columnNamePanel.add(updateCheckBox);
		columnNamePanel.add(queryCheckBox);
	}

	private JSpinner initJspinner(JSpinner spinner) {
		spinner.setModel(new SpinnerNumberModel(1, 1, 30, 1));
		JSpinner.NumberEditor editor = new JSpinner.NumberEditor(spinner, "0");
		spinner.setEditor(editor);
		JFormattedTextField formattedTextField = ((JSpinner.NumberEditor) spinner.getEditor()).getTextField();
		formattedTextField.setEditable(true);
		DefaultFormatterFactory factory = (DefaultFormatterFactory) formattedTextField.getFormatterFactory();
		NumberFormatter formatter = (NumberFormatter) factory.getDefaultFormatter();
		// 设置输入限制生效
		formatter.setAllowsInvalid(false);
		return spinner;
	}

	/**
	 * 获取主键字符串
	 *
	 * @param primaryKeyListText
	 * @param primaryList
	 */
	private void getAndSetPkStr(JTextField primaryKeyListText, List<String> primaryList) {
		StringBuilder primaryKeyStr = new StringBuilder();
		if (primaryList != null) {
			for (int i = 0; i < primaryList.size(); i++) {
				if (i == primaryList.size() - 1) {
					primaryKeyStr.append(primaryList.get(i));
					break;
				}
				primaryKeyStr.append(primaryList.get(i)).append("#");
			}
		}
		primaryKeyListText.setText(primaryKeyStr.toString());
	}

	/**
	 * 设置配置说明 listener
	 *
	 * @param configIntroduce
	 */
	public void setConfigIntroduceListener(JButton configIntroduce) {
		configIntroduce.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				JOptionPane.showMessageDialog(configIntroduce.getParent(),
						"查询次序：请按照字段被查询所能筛选掉数据的个数进行设置，把能筛选掉大量数据的字段的按升序设置，将根据设置自动对sql的查询顺序进行优化！" + CodeConstant.NEW_LINE
								+ "业务类型：请根据字段的具体含义选择业务类型" + CodeConstant.NEW_LINE + "类型描述：" + CodeConstant.NEW_LINE
								+ "    布尔示例：是#否 其中是和否填写数据库中用来表示是和否的值" + CodeConstant.NEW_LINE
								+ "    状态码示例：状态名称&状态值#状态名称&状态值（填写状态名称和状态值便于前台生成样式）" + CodeConstant.NEW_LINE
								+ "    其他类型将根据类型生成对应的样式，无需描述" + CodeConstant.NEW_LINE + "更新展示：生成的页面点击更新按钮的时候需要展示的字段"
								+ CodeConstant.NEW_LINE + "条件展示：生成的页面上的查询条件需要展示的字段",
						"提示", JOptionPane.INFORMATION_MESSAGE);
			}
		});
	}


}
